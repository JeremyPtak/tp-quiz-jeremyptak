import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css']
})
export class TimerComponent implements OnInit {
  timer;

  constructor() { 
    this.timer=10;
  }

  ngOnInit() {
    console.log(this.timer);
    let traitementRegulier = setInterval (()  =>  {},  100 );

    clearInterval(traitementRegulier)
  }
}
