import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TimerComponent } from './timer/timer.component';
import { ScoreComponent } from './score/score.component';
import { QuizComponent } from './quiz/quiz.component';

const routes: Routes = [
  //{ path: "", redirectTo="app"},
  { path: "quiz", component: QuizComponent },
  { path: "score", component: ScoreComponent },
  { path: "timer", component: TimerComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
